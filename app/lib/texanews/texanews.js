function getNews(onSuccess){
	var url = "http://api.espn.com/v1/sports/football/nfl/news/?teams=34&apikey=gx4g3twpxt5xu8kt39yw59e9";
	// Ti.API.info("Getting Texans news");
	var client = Ti.Network.createHTTPClient({
		onload: function(e) {
			json = JSON.parse(this.responseText);
			//Ti.API.info(this.responseText);
			Ti.API.debug(json);
			onSuccess(json);
		},
		onerror: function(e){
			Ti.API.debug(e.error);
			alert("Could not retrieve news!");
		}
	});
	client.open("GET", url);
	client.send();
}


var news = {
	getNews: function(onSuccess){
		return getNews(onSuccess);
	}
};
module.exports = news;