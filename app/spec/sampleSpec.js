var newsService = require("../lib/texanews/texanews");

describe('sample behavior specification', function() {
	
	it('should spy our service', function() {
		
		spyOn(newsService, 'getNews').andReturn(true);
		expect( newsService.getNews() ).toBeTruthy(); 
		
	});
	
	it('should mock our service', function() {
		
		spyOn(newsService, 'getNews').andReturn(
			'{"headlines":[{"headline":"TestHeadline","published":"2014-05-27T19:39:21Z","links":{"mobile":{"href":"http://www.espn.com"}},"images":[{"url":"http://n7ugc.disney.go.com/item/hungergames0909/610_2gs11k6Xov%5EG000010048g00-g-d98e10-as/rate%205%20if%20scared%20of%20sharks__600_450_q50.jpg"}]}]}'
		);
		
		var newsObj = JSON.parse( newsService.getNews() );
		//console.log(newsObj);
		
		// object should have 'headlines'
		expect(newsObj.headlines).toBeDefined();
		
		// headlines should have at least 1 item
		expect(newsObj.headlines instanceof Array).toBeTruthy();
		expect(newsObj.headlines.length > 0).toBeTruthy();
		
		// that item should have a headline
		
		// that item should have 'published'
		
		// ...
		
	});
	
});
