var newsService = require("texanews/texanews");

$.index.open();

function init() {
	refresh();
}

function refresh() {
	newsService.getNews(function(retInformation) {
		clearList($.newsList);
		loadList($.newsList, retInformation);
	});
}

function clearList(list) {
	// clears the list and adds the header table row back in
	list.data = [];
}

function loadList(list, info) {
	var rowsToAdd = new Array();
	// For each headline we get, create a new row and add it to the table
	info.headlines.forEach(function(headline) {
		var title = headline.headline;
		var date = new Date(headline.published);
		var link = headline.links.mobile.href;
		var imgPath = "http://www.dolphins-world.com/wp-content/uploads/dolphin-main.jpg";
		if (headline.images instanceof Array && headline.images.length >= 1) {
			imgPath = headline.images[0].url;
		}
		
		var row = Ti.UI.createTableViewRow({
			height: "auto"
		});
		
		var rowView = Ti.UI.createView({
			layout: "horizontal"
		});
		
		var leftView = Ti.UI.createView({
			layout: "vertical",
			height: "auto"
		});
		$.addClass(leftView, 'newsLeft');
		var imageView = Ti.UI.createImageView({
			image: imgPath
		});
		$.addClass(imageView, 'newsImage');
		var dateLabel = Ti.UI.createLabel({
			text: String.formatDate(date, "long")
		});
		$.addClass(dateLabel, 'newsDate');
		var headlineLabel = Ti.UI.createLabel({
			text: title
		});
		$.addClass(headlineLabel, 'newsHeadline');
		
		leftView.add(imageView);
		leftView.add(dateLabel);
		rowView.add(leftView);
		rowView.add(headlineLabel);
		row.add(rowView);
		
		// On click go to the website
		row.addEventListener('click', function(evt) {
			Ti.API.info('clicked for ' + link + '!');
			Ti.Platform.openURL(link);
		});
		
		rowsToAdd.push(row);
	});
	
	list.appendRow(rowsToAdd);
}

init();
